function diCaprio(object) {
  const array = Object.entries(object);

  const result = array.filter((value) => {
    const item = value[1]["actors"].includes("Leonardo Dicaprio");

    if (item) {
      return value;
    }
  });
  return result;
}

module.exports = diCaprio;
