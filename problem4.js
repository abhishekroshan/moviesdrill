function imdbRating(object) {
  const array = Object.entries(object);

  const result = array.sort((itemA, itemB) => {
    const valueA = itemA[1].imdbRating;
    const valueB = itemB[1].imdbRating;

    return valueA - valueB;
    //sorting based on imdb rating lower -->> higher
  });
  return result;
}

module.exports = imdbRating;
