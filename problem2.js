function oscarNomination(object) {
  const array = Object.entries(object);

  const result = array.filter((value) => {
    const earning = Number(
      value[1].totalEarnings.replace("$", "").replace("M", "")
    );
    /*Replacing '$' and 'M' with "" and converting it into a number*/

    const totalOscar = value[1].oscarNominations;

    if (earning > 500 && totalOscar > 3) {
      return value;
    }
    //checking if the both conditions satisfy.
  });
  return result;
}

module.exports = oscarNomination;
