function genreBased(object) {
  const array = Object.entries(object);
  const result = array.reduce((accumulator, currentValue) => {
    const value = currentValue[1].genre.forEach((element) => {
      if (!accumulator[element]) {
        accumulator[element] = [];
      }
      /*if the name is not present in the object we 
      create it with an empty array 
      */

      accumulator[element].push(currentValue[0]);
      //push the element into the corrosponding array.
    });
    return accumulator;
  }, {});
  return result;
}

module.exports = genreBased;
