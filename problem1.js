function totalEarning(object) {
  const array = Object.entries(object);

  const result = array.reduce((accumulator, currentValue) => {
    const earning = Number(
      currentValue[1].totalEarnings.replace("$", "").replace("M", "")
    );
    /* 
    removing the character and symbol from the string of totalEarning
    and comverting it into a number.
    */

    if (earning > 500) {
      accumulator.push(currentValue);
    }
    return accumulator;
  }, []);
  return result;
}

module.exports = totalEarning;
